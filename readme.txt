==========================
		TASK 1
==========================
 Main Description:

 1) Create 3 Classes which describe Employee. One Abstract Class and two concrete implementations.
    First implementation - Employee with fixed salary. Where average monthly salary = employee salary.
    Second implementation - Employee with per-hour salary. Where average monthly salary = 20.8 8 employee salary.

    In Abstract Employee Class describe an abstract method which calculates Employee average monthly salary
    according to rules described above(method:getSalary).

 2) Create Class which represents collection of Employees.
    1. Collection of Employees must be sorted by the next rules:
       Sort all workers in descending order of average monthly salary.
       If average monthly salary of Employees is equal use Employee name instead.
    2. Ability to get id, name, average monthly salary for each Employee in collection.
       Output example:
       [
       {id: employeeId, name: employeeName, salary: employee average monthly salary},
       {id: employeeId, name: employeeName, salary: employee average monthly salary}
       ]
    3. Ability to get five first employee names from collection.
       Output example:
       ['Jo', 'Bob', 'Alice', 'Robb', 'Jenny']
    4. Ability to get last three employee ids from collection.
       Output example:
       ['id5', 'id4', 'id3']

 3) Organize ability to get Employees Data from different sources (AJAX, Textarea on the page).
    Note here:
    Using the same Collection Class we want to have an ability to get data from Back End in one place but in another place
    we want to get data from text area on the page(Lets imagine that it's a kind of admin tool).

 4) Protect your classes from incorrect input. Meaningful error handling.



 Additional notes:
 You can use lodash/underscore libs.
 jQuery for DOM manipulations/AJAX if needed.
 If you want to use Async Flow Control use Q, jQuery.Deferred(). Q is preferable.
 MVC frameworks are prohibited here.

 Optional:
 Use AMD(Require.js for example).
 Unit tests(Any framework).

==========================
		TASK 2
==========================

The main goal for this task is to replace your mocked data with real remote storage.
For the first part we’ll use Firebase storage, for the second there will be real REST service.

Description:

1. Set up firebase storage 
	1.1. Create account on https://www.firebase.com
	1.2. Read https://www.firebase.com/docs/web/quickstart.html and include script with client library to your app.

2. Read the guide - https://www.firebase.com/docs/web/guide/

3. Implement form with all appropriate fields for adding/editing employees and “SAVE” button. 

4. Implement CRUD (http://en.wikipedia.org/wiki/Create,_read,_update_and_delete) operations for your Employees collection
	4.1 You should implement some interactive table with “EDIT” and “DELETE” buttons text to each employee.
	4.2. Clicking on “EDIT” button should fill appropriate fields in form.

5. Add “LOAD” button somewhere for current collection state loading.

Notes:
Usage of any jQuery plugins is allowed.
Usage of any MV* frameworks is allowed.