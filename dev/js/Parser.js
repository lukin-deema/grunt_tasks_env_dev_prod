(function(global) {

    function Parser(nodes) {
        this.textareaButton = nodes.textareaButton;
        this.inputForWebButton = nodes.inputForWebButton;
        this.getInfoButton = nodes.getInfoButton;
        this.getTopNames = nodes.getTopNames;
        this.topNamesInput = nodes.topNamesInput;
        this.getLastIdsButton = nodes.getLastIdsButton;
        this.lastIdsInput = nodes.lastIdsInput;
        this.inputForWeb = nodes.inputForWeb;
        this.textarea = nodes.textarea;
        this.output = nodes.output;
        this.firebaseAddButton = nodes.firebaseAddButton;
        this.firebaseLoadButton = nodes.firebaseLoadButton;
        this.clearOutput = nodes.clearOutput;
    }

    Parser.prototype.init = function() {

        var collection = new EmployeesCollection();
        var firebaseCollection = new EmployeesCollection();
        var FIREBASE_URL = 'https://employee-collection.firebaseio.com/';
        var $salary = $('#salary');
        var $name = $('#name');
        var $ID = $('#ID');
        var ENTER_KEY = 13;
        var $table = $('#employeesTable');
        var $tableBody = $table.find('tbody');

        function decorateWithHighlight() {
            $('code').each(function(i, block) {
                hljs.highlightBlock(block);
            });
        }

        firebaseCollection.getData('json', FIREBASE_URL + '.json', function() {
            firebaseCollection.employees.map(function(employee, i) {
                firebaseCollection.employees[i] = new EmployeeFactory().createEmployee(employee);
            });
        });

        this.textareaButton.click(function() {
            if (this.textarea.val()) {
                collection.getData('html', this.textarea.val());
                this.output.append($('<div><code class="hljs json">' + JSON.stringify(collection.employees) + '</code></div>'));
            }
            decorateWithHighlight();
        }.bind(this));

        this.inputForWebButton.click(function() {
            function cb() {
                this.output.append($('<div><code class="hljs json">' + JSON.stringify(collection.employees) + '</code></div>'));
                decorateWithHighlight();
            }
            collection.getData('json', this.inputForWeb.val(), cb.bind(this));
        }.bind(this));

        this.getInfoButton.click(function() {
            this.output.append($('<div><code class="hljs json">' + JSON.stringify(collection.getInfo()) + '</code></div>'));
            decorateWithHighlight();
        }.bind(this));

        this.getTopNames.click(function() {
            this.output.append($('<div><code class="hljs">' + JSON.stringify(collection.getTopNames(this.topNamesInput.val())) + '</code></div>'));
            decorateWithHighlight();
        }.bind(this));

        this.getLastIdsButton.click(function() {
            this.output.append($('<div><code class="hljs">' + JSON.stringify(collection.getLastIds(this.lastIdsInput.val())) + '</code></div>'));
            decorateWithHighlight();
        }.bind(this));

        this.firebaseAddButton.submit(function(event) {
            event.preventDefault();
            var myDataRef = new Firebase(FIREBASE_URL);
            var data = {
                'type': $('input:checked', '#addEmployee').parent().text(),
                'salary': Number($salary.val()) || 'unspecified',
                'name': $name.val() || 'anonymous',
                'id': Number($ID.val()) || 'unspecified'
            };
            var key = myDataRef.push(data, function() {
                firebaseCollection.addEmployee(FIREBASE_URL + key + '/.json', key);
                $salary.val('');
                $name.val('');
                $ID.val('');
            }).key();
        });

        this.firebaseLoadButton.click(function() {
            var $row;

            function toggleButtonState(button) {
                var $thisFieldsToEdit = button.closest('tr').find('.employee-name, .employee-salary, .employee-id');
                if (button.text() === 'EDIT') {
                    button.text('SAVE')
                        .addClass('not-saved')
                        .one('click', function() {
                            var thisIndex = $(this).closest('tr').index();
                            var thisEmployee = firebaseCollection.employees[thisIndex];
                            var uniqueID = button.data('firebaseUniqueId');
                            var firebaseRef = new Firebase(FIREBASE_URL + uniqueID);
                            var updateData = {
                                name: $thisFieldsToEdit.eq(1).text(),
                                salary: thisEmployee instanceof HourlySalaryEmployee ? (Number($thisFieldsToEdit.eq(2).text()) / 20.8 / 8 || null) : (Number($thisFieldsToEdit.eq(2).text()) || null),
                                id: Number($thisFieldsToEdit.eq(0).text()) || null,
                            };
                            firebaseRef.update(updateData, function() {
                                thisEmployee.name = updateData.name;
                                thisEmployee.salary = updateData.salary;
                                thisEmployee.id = updateData.id;
                            });
                        });
                    $thisFieldsToEdit.attr('contenteditable', 'true')
                        .keypress(function(event) {
                            if (event.which === ENTER_KEY) {
                                event.preventDefault();
                                $(this).next().focus();
                            }
                        });
                } else {
                    button.text('EDIT')
                        .removeClass('not-saved');
                    $thisFieldsToEdit.attr('contenteditable', 'false');
                }
            }
            $tableBody.empty();
            $table.hide()
                .fadeIn();

            firebaseCollection.employees.forEach(function(employee) {
                $row = $('<tr>' +
                    '<td class="employee-id">' + employee.id + '</td>' +
                    '<td class="employee-name">' + employee.name + '</td>' +
                    '<td class="employee-salary">' + (Math.round(employee.getSalary()) || 'unspecified') + '</td>' +

                    '<td><button class="edit" data-firebase-unique-id="' + employee['firebaseUniqueId'] + '">EDIT</button></td>' +
                    '<td><button class="delete" data-firebase-unique-id="' + employee['firebaseUniqueId'] + '">DELETE</button></td>' +
                    '</tr>');
                $row.appendTo($tableBody);
            });

            $('.delete').click(function() {
                var thisObjAtFirebase;
                var thisIndex = $(this).closest('tr').index();
                var thisFirebaseUniqueId = $(this).data('firebaseUniqueId');
                firebaseCollection.employees.splice(thisIndex, 1);
                thisObjAtFirebase = new Firebase(FIREBASE_URL + thisFirebaseUniqueId);
                thisObjAtFirebase.remove();
                $(this).closest('tr').remove();
            });
            $('.edit').click(function() {
                toggleButtonState($(this));
                $(this).closest('tr').find('.employee-name').focus();
            });
        }.bind(this));
        this.clearOutput.click(function() {
            $table.fadeOut()
                .siblings('div').remove();
        });
    };

    var nodesForParser = {
        textareaButton: $('#getDataArea'),
        inputForWebButton: $('#getDataWeb'),
        getInfoButton: $('#getInfo'),
        getTopNames: $('#getTop5'),
        topNamesInput: $('#topNames'),
        getLastIdsButton: $('#getLast3Ids'),
        lastIdsInput: $('#lastIds'),
        inputForWeb: $('#webSource'),
        textarea: $('textarea'),
        output: $('.output'),
        firebaseAddButton: $('#addEmployee'),
        firebaseLoadButton: $('#load'),
        clearOutput: $('#clearButton')
    };

    global.parser = new Parser(nodesForParser);
    parser.init();

})(this);
hljs.initHighlightingOnLoad();