(function(global) {
    function HourlySalaryEmployee(parameters) {
        this.salary = parameters.salary;
        this.name = parameters.name;
        this.id = parameters.id;
        if (parameters.firebaseUniqueId) {
            this.firebaseUniqueId = parameters.firebaseUniqueId;
        }
    }
    HourlySalaryEmployee.prototype = Object.create(Employee.prototype, {
        getSalary: {
            value: function() {
                return this.salary * 20.8 * 8;
            }
        },
        constructor: {
            value: HourlySalaryEmployee
        }
    });
    global.HourlySalaryEmployee = HourlySalaryEmployee;
})(this);