(function(global) {
    function Employee() {}
    Employee.prototype.getSalary = function() {
        throw new Error("You should define this method in the class before use.");
    };
    global.Employee = Employee;
})(this);